EXIF Manipulate
===============

This module strips out EXIF metadata from images upon upload. Modern cameras and phones include data in the images that
they produce that may be privacy-sensitive, like location. It is a common requirement to strip out such information.
It is intended to extend this module with extension points so that it is possible to not only strip out unwanted data,
but also insert new data.

Note that the orientation information used by [EXIF Orientation](https://drupal.org/project/exif_orientation) is left
unaffected.

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/exif_manipulate).


## Requirements

This module requires the [fileeye/pel](https://github.com/FileEye/pel) PHP library. When installing the module using
Composer (recommended), this will be installed automatically.

## Recommended modules

It is recommended to install [EXIF orientation](https://drupal.org/project/exif_orientation), which will rotate uploaded
files according to the rotation information.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Optionally, visit the conversion form at /admin/config/media/exif_manipulate/form; this will allow you to
   retroactively apply the module's logic on certain file system locations.

## Maintainers

- Ahmet Burkan - [ahmetburkan](https://www.drupal.org/u/ahmetburkan)
- Eelke Blok - [eelkeblok](https://www.drupal.org/u/eelkeblok)
- Jaap Jan Koster - [jaapjan](https://www.drupal.org/u/jaapjan)
