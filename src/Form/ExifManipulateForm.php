<?php

namespace Drupal\exif_manipulate\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExifManipulateForm to clean exif data form file directory.
 */
class ExifManipulateForm extends ConfirmFormBase {

  /**
   * The files to be cleaned.
   *
   * @var array
   */
  protected array $files;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('queue'),
      $container->get('state')
    );
  }

  /**
   * Constructs a new object.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   Queue factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   */
  public function __construct(
    protected QueueFactory $queueFactory,
    protected StateInterface $state
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'exif_manipulate_clean_images_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to clean the EXIF data of the directory?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Clean images');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('system.admin_config_media');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $total = $this->state->get('exif_manipulate_clean_exif_data_total', 0);

    if ($total > 0) {
      $queue = $this->queueFactory->get('exif_manipulate_clean_exif_data');
      $entitiesLeft = $queue->numberOfItems();

      $progress = $total - $entitiesLeft;
      // When we have a longer queue than the total items, we get weird results
      // with a negative progress. This makes it slightly less weird, although
      // the total reported is still that of the last directory scanned.
      if ($progress < 0) {
        $progress = 0;
      }

      if ($progress < $total) {
        $this->messenger()->addWarning($this->t(
          'Clean-up in progress, @progress of @total (@percentage %) of files cleaned.',
          [
            '@progress' => $progress,
            '@total' => $total,
            '@percentage' => intval($progress / $total * 100),
          ]));
      }
    }

    $form['page_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Clean EXIF data from uploads'),
      '#open' => TRUE,
    ];

    $form['page_settings']['directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Directory'),
      '#required' => TRUE,
      '#default_value' => 'sites/default/files',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Clean directory'),
      '#button_type' => 'primary',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $directory = trim($form_state->getValue('directory'), '/');

    if (!is_dir($directory)) {
      $form_state->setErrorByName($directory, $this->t("The path '%path' is not a directory.", ['%path' => $directory]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $directory = trim($values['directory'], '/');
    $files = $this->getFilesInDir($directory);
    $queue = $this->queueFactory->get('exif_manipulate_clean_exif_data');

    if ($files) {
      foreach ($files as $file) {
        $queue->createItem(['uri' => $file]);
      }

      $this->state->set('exif_manipulate_clean_exif_data_total', count($files));
      $this->messenger()->addStatus($this->t('The images have been queued for cleaning.'));
      $form_state->setRedirect('exif_manipulate.clean_images_form');
    }
  }

  /**
   * Returns the files that will be cleaned.
   *
   * @param string $directory
   *   The directory of the images.
   *
   * @return array|null
   *   The files to be cleaned or NULL if there are none.
   */
  public function getFilesInDir($directory): ?array {
    $directoryScanned = scandir($directory);

    if (!$directoryScanned) {
      return $this->files;
    }

    foreach ($directoryScanned as $content) {
      $uri = NULL;
      // Skip these 2 results.
      if ($content === '.' | $content === "..") {
        continue;
      }

      // Convert to uri.
      if (str_contains($directory, 'sites/')) {
        $basePath = PublicStream::basePath() . '/';
        $uri = str_replace($basePath, 'public://', $directory . '/' . $content);
      }

      // Is already an uri.
      if (str_contains($directory, 'public://')) {
        $uri = $directory . '/' . $content;
      }

      // If content is a directory get the files.
      if (is_dir($uri)) {
        $this->getFilesInDir($uri);
      }

      // Add uri to files.
      if (is_file($uri)) {
        $this->files[] = $uri;
      }
    }

    return $this->files;
  }

}
