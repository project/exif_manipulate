<?php

namespace Drupal\exif_manipulate\Services;

use Drupal\file\FileInterface;
use lsolesen\pel\PelEntryShort;
use lsolesen\pel\PelExif;
use lsolesen\pel\PelIfd;
use lsolesen\pel\PelJpeg;
use lsolesen\pel\PelTag;
use lsolesen\pel\PelTiff;

/**
 * Class FileProcessorInterface processes EXIF data for files.
 * */
class FileExifProcessor implements FileExifProcessorInterface {

  /**
   * {@inheritDoc}
   */
  public function manipulate(FileInterface $file): void {
    $mimeType = $file->getMimeType();
    $uri = $file->uri->value;

    if (isset($mimeType)) {
      $pelFile = $this->getFile($mimeType, $uri);

      if ($pelFile) {
        $exif = $pelFile->getExif();

        if (isset($exif)) {
          $originalOrientation = $this->getOrientation($exif);
          $pelFile->clearExif();
          if (!is_null($originalOrientation)) {
            $this->setOrientation($originalOrientation, $pelFile);
          }
          $pelFile->saveFile($uri);
        }
      }
    }
  }

  /**
   * Returns the PEL file object for the image file.
   *
   * @param string $mimeType
   *   The mimeType of the image.
   * @param string $uri
   *   The uri of the image.
   *
   * @return null|\lsolesen\pel\PelJpeg|\lsolesen\pel\PelTiff
   *   A PEL file object, or NULL if it cannot be opened.
   *
   * @throws \lsolesen\pel\PelInvalidArgumentException
   *   Throws error if the parameter is incorrect.
   */
  protected function getFile(string $mimeType, string $uri): null|PelTiff|PelJpeg {
    $pelFile = match ($mimeType) {
      'image/jpeg' => new PelJpeg($uri),
      'image/tiff' => new PelTiff($uri),
      default => NULL,
    };

    return $pelFile;
  }

  /**
   * Gets exif orientation.
   *
   * @param \lsolesen\pel\PelExif $exif
   *   The PelExif parameter.
   *
   * @return \lsolesen\pel\PelEntryShort|null
   *   Returns the orientation data or NULL.
   */
  protected function getOrientation(PelExif $exif): ?PelEntryShort {
    $tiff = $exif->getTiff();
    if (!isset($tiff)) {
      return NULL;
    }

    $ifd0 = $tiff->getIfd();
    if (!isset($ifd0)) {
      return NULL;
    }

    $orientation = $ifd0->getEntry(PelTag::ORIENTATION);
    if (!isset($orientation)) {
      return NULL;
    }

    return new PelEntryShort($orientation->getTag(), $orientation->getValue());
  }

  /**
   * Sets exif orientation.
   *
   * @param \lsolesen\pel\PelEntryShort $entry
   *   The pelEntryShort parameter.
   * @param $pelFile
   *   The file to manipulate.
   *
   * @throws \lsolesen\pel\PelInvalidDataException
   *   Throws error if the parameter is incorrect.
   */
  protected function setOrientation(PelEntryShort $entry, $pelFile): void {
    $exif = $pelFile->getExif();
    if (!isset($exif)) {
      $exif = new PelExif();
    }

    $pelFile->setExif($exif);

    $tiff = new PelTiff();
    $exif->setTiff($tiff);

    $ifd0 = new PelIfd(PelIfd::IFD0);
    $tiff->setIfd($ifd0);

    $ifd0->addEntry($entry);
  }

}
