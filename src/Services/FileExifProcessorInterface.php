<?php

namespace Drupal\exif_manipulate\Services;

use Drupal\file\FileInterface;

/**
 * Class FileProcessorInterface processes EXIF data for files.
 */
interface FileExifProcessorInterface {

  /**
   * Manipulate EXIF file data.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity.
   */
  public function manipulate(FileInterface $file): void;

}
