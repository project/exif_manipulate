<?php

namespace Drupal\exif_manipulate\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\Utility\Error;
use Drupal\exif_manipulate\Services\FileExifProcessorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Cleans Exif data from images.
 *
 * @QueueWorker(
 *   id = "exif_manipulate_clean_exif_data",
 *   title = @Translation("Clean Exif data"),
 *   cron = {"time" = 60}
 * )
 */
class CleanExifDataQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * File Exif Processor.
   *
   * @var \Drupal\exif_manipulate\Services\FileExifProcessorInterface
   */
  protected $fileExifProcessor;

  /**
   * Our logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('exif_manipulate.file_exif_processor'),
      $container->get('logger.channel.exif_manipulate')
    );
  }

  /**
   * UpdateStatusCollector constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\exif_manipulate\Services\FileExifProcessorInterface $fileExifProcessor
   *   The file Exif processor service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   A logger channel.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    FileExifProcessorInterface $fileExifProcessor,
    LoggerChannelInterface $logger
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->fileExifProcessor = $fileExifProcessor;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    try {
      $files = $this->entityTypeManager->getStorage('file')
        ->loadByProperties(['uri' => $data['uri']]);
    }
    catch (\Exception $e) {
      Error::logException($this->logger, $e);

      return;
    }

    if (!empty($files)) {
      /** @var \Drupal\file\FileInterface $file */
      $file = reset($files);
      $this->fileExifProcessor->manipulate($file);
    }
  }

}
